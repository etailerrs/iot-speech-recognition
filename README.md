# IoT Speech Recognition

### Chrome Speech API

Google Chrome has built-in Speech Recognition API available through JavaScript. At the moment, this API works only in Chrome browser. Here is simple PoC using Serbian language to control IoT device over MQTT protocol and Message Broker. When voice command "turn on/turn off" is recognized - relevant MQTT message is sent to the IoT device.

The API will listen in real time for the conversation in Serbian language. If it detects "turn on" string in the conversation:

"Turn on the light" or
"You should turn on..." or
"... Whatever... turn on..."



